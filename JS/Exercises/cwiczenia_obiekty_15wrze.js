/* 
 * http://palo.ferajna.org/sda/py/zad2.pdf
 * https://repl.it/DaJB/19
 */

//wyswietla adresy mieszkan w n blokach na danej ulicy
function cw1(nazUlicy, ostNumer) {
    var adresy = [];
    var adres = "";
    var klatka = '';
    for (var i = 1; i <= ostNumer; i += 2) {
        for (var j = 1; j <= 12; j++) {
            (j < 7) ? klatka = "A" : klatka = "B";
            //console.log("ul." + nazUlicy + " " + i + klatka + "/" + j);
            adres = "ul." + nazUlicy + " " + i + klatka + "/" + j;
            adresy.push(adres);
        }
    }
    return adresy;
}

adresy = cw1("Przykładowa", 15);
function cw2(arg) {

    arg = arg.toString();
    arg = arg.split(" ").join("");
    for (var i = 0; i < Math.floor(arg.length / 2); i++) {
        if (arg[i] != arg[arg.length - 1 - i]) {

            return false;
        }
    }
    return true;
}

cw2([6, 78, 78, 6]);
function cw3(pesel) {
    var day = parseInt(pesel.substring(4, 6)),
            month = parseInt(pesel.substring(2, 4)),
            year = 1900 + parseInt(pesel.substring(0, 2)),
            sex = parseInt(pesel[9]) % 2 == 0 ? "Kobieta" : "Mężczyzna";
    if (month > 20) {
        month -= 20;
        year += 100;
    }
    month = addZero(month, 2);
    day = addZero(day, 2);
    var wagi = [1, 3, 7, 9];
    var suma = 0;
    for (var i = 0; i < 10; i++) {
        suma += parseInt(pesel[i]) * wagi[i % 4];
    }
    console.log(pesel[10] == (10 - suma % 10));
    return day + "." + month + "." + year + ", " + sex;
}

cw3('44050401358');
function addZero(arg, num) {
    var str = '';
    arg = arg.toString();
    if (num - arg.length > 0) {
        arg = "0".repeat(num - arg.length) + arg;
    }
    return arg;
}

///////////szyfr cezara
//szyfrowanie
function cw4(text, offset, type) {
    //kodujaca
    if (type == 'c') {
        return cw4a(text, offset);
    }
    //deszyfrujaca
    else if (type == 'd') {
        offset = -offset;
        return cw4a(text, offset);
    }
    return "zły typ";

}
cw4('fchlyj spuylw', 2, 'c');

function cw4a(text, offset) {
    var alphabet = '';
    var code = '';
    for (var i = 97; i <= 122; i++) {
        alphabet += String.fromCharCode(i);
    }
    //for(var i in text) {

    //code += alphabet[(alphabet.indexOf(text[i]) + offset)%alphabet.length];

    //}

    //tworzymy tablice z przesunietym alfabetem
    alphabet = alphabet.split('');
    var cipher = alphabet.slice(offset);
    cipher = cipher.concat(alphabet.slice(0, offset));

    for (var i = 0; i < text.length; i++) {
        if (alphabet.indexOf(text[i]) == -1) {
            code += text[i];
        } else {
            code += cipher[alphabet.indexOf(text[i])];
        }
    }
    return code;
}


function cw5(text) {
    var tempStr = '';
    var alphabet = '';
    var code = '';
    for (var i = 97; i <= 122; i++) {
        alphabet += String.fromCharCode(i);
    }
    cipher = alphabet.substring(13) + alphabet.substring(0, 13);
    for (var i = 0; i < text.length; i++) {
        if (alphabet.indexOf(text[i]) == -1) {
            code += text[i];
        } else {
            code += cipher[(alphabet.indexOf(text[i]))];
        }
    }
    return code;
    // for(var j=0; j<text.length; j++) {
    // 	if(alphabet.indexOf(text[j])==-1) {
    // 		code+= text[j];
    // 	} else
    // 		code += alphabet[(alphabet.indexOf(text[j]) + 13)%alphabet.length];
    // 	}
    // return code;
}

cw5('hejnal urwany');

String.prototype.spacjuj = function () {
    return this.split('').join(' ');
};

function cw7a() {
    //var time = prompt("Podaj maksymalną liczbę zgadnięć: ");
    //przedzial (a, b)
    var liczba = losujLiczbe(20, 30);
    var liczba1 = losujLiczbe(10, 100);
    var liczba2 = losujLiczbe(50, 55);
    var liczba3 = losujLiczbe(100, 200);
    var liczba4 = losujLiczbe(500, 1000);
    var liczba5 = losujLiczbe(-25, -5);
    console.log(liczba1);
    console.log(liczba2);
    console.log(liczba3);
    console.log(liczba4);
    console.log(liczba5);
    console.log(losujLiczbe(-50, 50));
    //jak zrobic sume lub roznice przedzialow?
    console.log(losujLiczbe(-10, 0) + losujLiczbe(10, 100));
    console.log(losujLiczbe(10, 15) - losujLiczbe(5, 10));
    console.log(Math.round((5 + Math.random() * (10 - 5)) - (10 + Math.random() * (15 - 10))));
    // var liczba6 = losujLiczbe(-10,100);
    // if( liczba6 > 0 && liczba6 < 10) {
    // 	liczba6 = losujLiczbe(-10,100);
    // }
    // console.log(liczba6);

    //zgadywanie
}

cw7a();

function losujLiczbe(a, b) {
    var liczba = Math.round(a + Math.random() * (b - a));
    return liczba;
}

function cw7() {
    //a musi byc mniejsze niz b
    var a = parseInt(prompt("Podaj liczbę a")),
            b = a, n = 0;
    var guess;
    while (b <= a) {
        b = parseInt(prompt("Podaj liczbę b (musi być większa niż a)"));
    }
    while (n <= 0) {
        n = parseInt(prompt("Podaj liczbę prób zgadnięcia (n>1)"));
    }
    console.log(a, b, n);
    //losowanie 
    var liczba = losujLiczbe(a, b);

    //zgadywanie1
    var counter = 0;
    do {
        guess = parseInt(prompt("zgaduj liczbę"));
        if (guess == liczba) {
            alert('brawo');
            return true;
        }
        counter++;
    } while (guess != liczba && counter < n);
    alert("przegrana, liczba to " + liczba);

    //zgadywanie2
    for (var i = 1; i <= n; i++) {
        guess = parseInt(prompt("zgaduj liczbę"));
        if (guess == liczba) {
            alert('brawo! zgadłeś w ' + i + " próbach");
            return;
        } else if (guess != liczba && i == n) {
            alert('Przegrana. Szukana liczba to ' + liczba);
        } else if (guess > liczba) {
            alert('mniej!');

        } else {
            alert('więcej!');
        }
    }
}

cw7();

function losujLiczbe(a, b) {
    var liczba = Math.round(a + Math.random() * (b - a));
    return liczba;
}

///// OBIEKTY
var obj = {
    'a': 1,
    'b': 'Ala ma kota',
    'func': function () {
        alert('Func');
    }
};

console.log(obj['a']);
console.log(obj.b);
console.log(obj.func);

//funkcja tworzaca obiekt
function createObject(a, b) {
    var obj = {};
    obj.a = a;
    obj.b = b;
    obj.func = function () {
        alert('FUnc');
    };

    return obj;
}
//instancja obiektu (onstancja obiektu matki)
//var MyObj = createObject(1, 'Ala ma kota');
var MyObj2 = createObject(2, 3);

//definiowanie obiektu za pomoca propotypu
function MyObject(msg) {
    if (msg) {
        this.sentence = msg;
    }

} // 1    //konstruktor - automatycznie wywolywana
MyObject.prototype.sentence = "hello";
MyObject.prototype.sayHi = function () //2 kazda instancja tworzona poprzez konstruktor MyObj
{
    alert(this.sentence); //3   mozna bedzie tworzyc takie same funkcje z roznymi parametrami
};

var oMyObj = new MyObject(); //4    tworzymy nowa instancje obiektu
//oMyObj.sayHi(); //5

var oMyObj3 = new MyObject("a ja nie bede taki jak inny");
var oMyObj4 = new MyObject();
oMyObj4.sentence = 'przyklad';
//oMyObj4.sayHi();
//oMyObj3.sayHi();

//parametryzacja konstruktora, rozserzenie typu obiektu (string.letterspacing)
//tworzenie obiektu za pomoca konstruktora
var MyObjConstr = function (a, b)
{
    this.func = function () {
        alert('ala ma kota');
    }  //funkcja anonimowa - bez nazwy
    this.a = a;
    this.b = b;
};

var MyObj = new MyObjConstr(1, 'hehe');
MyObj.func();
alert(MyObj.a);
MyObj.a = 10;
alert(MyObj.a);


///////////////////////////////////////////////////////////
var Pole = function (a, b) {
    this.a = a; /// robi sie this.a = a || 0;
    this.b = b;
    this.getPole = function () {
        if (a && !b) {
            return this.a * this.a;
        } else if (a && b) {
            return this.a * this.b;
        } else {
            console.log('błąd');
            return null;
        }
    };
    this.getObwod = function () {
        if (a && !b) {
            return this.a * 4;
        } else if (a && b) {
            return 2 * (this.a + this.b);
        } else {
            console.log('błąd');
            return null;
        }
    };
    this.getPrzekatna = function () {
        if (a && !b) {
            return Math.round(Math.sqrt(2) * (this.a) * 100) / 100;
        } else if (a && b) {
            return Math.round(Math.sqrt(this.a * this.a + this.b * this.b) * 100) / 100;
        } else {
            console.log('błąd');
            return null;
        }
    };
};

var oPoleKw = new Pole(1);
console.log('Pole kwadratu to: ' + oPoleKw.getPole());
var oPoleProst = new Pole(13, 41);
console.log('Pole prostokąta to: ' + oPoleProst.getPole());
console.log("Obwód kwadratu to: " + oPoleKw.getObwod());
console.log("Obwód prostokąta to: " + oPoleProst.getObwod());
console.log("Przekątna kwadratu to: " + oPoleKw.getPrzekatna());
console.log("Przekątna prostokąta to: " + oPoleProst.getPrzekatna());

/// .toFixed(2) zaokragla do dwoch miejsc po przecinku, ale zwraca stringa



///////
var Rand = function (a, b) {
    if (b < a) {
        this.a = b;
        this.b = a;
    } else {
        this.a = b;
        this.b = a;
    }

    this.getNrandomNums = function (n) {
        var tab = [];
        for (var i = 0; i < n; i++) {
            tab.push(this.losujLiczbe(a, b));
        }
        return tab;
    };
    this.generateNumberFromRange = function () {
        return this.losujLiczbe(a, b);
    };
    this.generateSmallNum = function () {
        return this.losujLiczbe(0, 11);
    };
    this.generateBigNum = function () {
        return this.losujLiczbe(100000, 10000001);
    };
    this.losujLiczbe = function (a, b) {
        var liczba = Math.round(a + Math.random() * (b - a));
        return liczba;
    };

};

var rand1 = new Rand(15, -10);
console.log(rand1.a, rand1.b);
console.log(rand1.getNrandomNums(5));
console.log(rand1.generateNumberFromRange());
console.log(rand1.generateSmallNum());
console.log(rand1.generateBigNum());

///opcja jezeli nie podajemy jej argumentow! uniwersalna funkcja
//this.generateNumberFromRange = function(c, d) {
// 	c = c || this.a;
// 	d = d || this.b;
// }