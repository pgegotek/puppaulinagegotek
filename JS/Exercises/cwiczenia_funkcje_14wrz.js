/* 
 * ciag dalszy
 * http://palo.ferajna.org/sda/py/zad15.pdf
 */

var x = prompt('Podaj liczby oddzielone przecinkiem');

cw_a(x);

function cw_a(x) {

    var tab = x.split(',');

    tab[0] = 5;
    for (var i in tab) {
        tab[i] = parseInt(tab[i]);
        console.log(tab[i], typeof tab[i]);
    }

}

x = 'programowanie';

//substring jest typem prostym, mozna uzywac: str.substring()
console.log(x.substring(10));
console.log(x.substring(3, 7));
console.log(x.substring(6, 10));
console.log(x.substring(1, 4) + x.substring(11, 12));//lepsze praktyka niz x[5] bo nie przypiszesz przypadkiem tam czegos
console.log(x.substring(4, 7) + x[5]);




function cw14(str) {
    str.split('');
    //bezpieczniejsze rozwiazanie
    var pierwsza = str.substring(0, 1);
    //var pierwsza = str[0];
    var tempStr = pierwsza;

    for (var i = 1; i < str.length; i++) {
        //if(str[i]==pierwsza) {
        //	tempStr += '_';//tempstr = tempstr + '_'
        //} else {
        //	tempStr += str[i];
        //}

        // lub:
        // if(str.substring(i, i+1) != pierwsza)
        // lub: 
        // split & join('');
        // lub:
        // str.split(str[0]) str.join('_'), return firstLetter + substring(1);
        str[i] == pierwsza ? (tempStr += '_') : (tempStr += str[i]);
    }
    return tempStr;
}

var str = 'oksymoron';
var str1 = 'rabarbar';
console.log(cw14(str));


function cw15(num, sen) {
    var tempArr = sen.split(" ");
    var retArr = [];
    for (var i in tempArr) {
        if (tempArr[i].length > num) {
            retArr.push(tempArr[i]);
        }
    }
    return retArr;
}

console.log(cw15(5, prompt("Wprowadź zdanie:")));

function cw16a(n) {
    var arr = [],
            counter = 0;
    var i = 0;
    if (n > 0) {
        for (var i = 0; i <= n; i = i + 2) //2* mniej operacji niz i++
        {
            arr.push(i);
            //console.log(counter);

            counter++;
        }
    }
    return arr;
}
var tab1 = cw16a(19), tab2 = [-1, 17, 9];

function cw16(arr1, arr2) {
    //arguments - tablica argumentow przekazanych do funkcji z indeksami arguments[0] itp
    for (var i in arr1) {
        if ((arr2.indexOf(arr1[i])) != -1) {
            return true;
        }

    }
    return false;
}

console.log(cw16(tab1, tab2));

///rekurencja :))))
function sumArrElems(arr) {
    var sum = 0;
    for (var i in arr) {
        if (typeof arr[i] == 'number') {

            sum += arr[i];
        } else if (typeof arr[i] == 'object') {
            sum += sumArrElems(arr[i]);
        }
    }
    return sum;
}

var tab1 = [1, 2, 4, [5, 6]];

sumArrElems(tab1);


function cw17(n) {
    var str = '';
    var newline = "\n";
    for (var i = 1; i <= n; i++) {
        console.log(' '.repeat(n - i) + '*'.repeat(i));

        //for(var j=1;j<=i;j++) {
        //	str += '*';
        //}
        //str += newline;
    }
    for (var i = n - 1; i >= 0; i--) {
        console.log(' '.repeat(n - i) + '*'.repeat(i));
    }
    return str;
}

cw17(3);

var x = 5;
var y = x;

//typ number przekazywany przez wartosc
x = 6;
console.log(y);

var x = [1, 2];
var y = x;

//typ object przekazywany jako referencja
y.push(3);
console.log(x);

var x = [5, 6, 7, 13, 45];

// [6,7]
// [13, 45]
// [5, 6, 7]

var a = x.slice(1, 3);
var b = x.slice(3);
var c = x.slice(0, 3);
var d = x.slice(-1);

//console.log(a, b, c, d);

// funkcja, ktora wypelni i zwroci tablice, tylko z liczbami,, ktore sa podzielne przez 7 lub przez 9
function fillArrayByDivs(n) {
    var arr = [];
    var tempArr = [];
    for (var i = 0; i <= n; i += 7) {
        arr.push(i);

    }
    for (var i = 0; i <= n; i += 9) {
        arr.push(i);

    }

    //odwrocenie tablicy
    tempArr = arr.reverse();
    // for(var i = arr.length;i>0;i--) {
    // 	tempArr.push(tab[i]);
    // }
    // var i = 0;
    // while(i<=n) {
    // 	if((i%7 == 0) || (i%9 == 0)) {
    // 		arr.push(i);

    // 	}
    // 	i++;
    // }

    return tempArr;
}

fillArrayByDivs(19);

//chaining (lancuchowanie)

function cw18(str) {
    //console.log(str.split('').reverse().join('')); 
    var tempStr = '';
    for (var i = str.length - 1; i >= 0; i--) {
        tempStr += str[i];
    }
    return tempStr;
}

cw18('oksymoron');

function cw19(n) {
    var arr = [];
    for (var i = 0; i <= n; i++) {
        if (i % 3 != 0 && i % 4 != 0) {
            arr.push(i);
        }
    }
    return arr;
}

cw19(16);