$(document).ready(function () {
    var $logBtn = $('#submit'),
            $register = $('#register'),
            $login = $('#login'),
            $container = $('.container'),
            $userData = $('userData'),
            $regName = $('#regName'),
            $newUser = $('#createNewUser'),
            $regBox = $('#regBox'),
            $regSname = $('#regSname'),
            $regPesel = $('#pesel'),
            $newUser = $('#newUser'),
            $userData = $('#userData');

    $regBox.hide();
    $userData.hide();
    // Definiujemy obiekt stosu

    var Stack = function () {
        var elems = [];

        function empty() {
            return (elems.length === 0) ? true : false;
        }

        function push(arg) {
            elems.push(arg);
        }

        function pop() {
            return (elems.length === 0) ? null : elems.pop();
        }

        function top() {
            return (elems.length === 0) ? null : elems.length - 1;
        }

        function size() {
            return elems.length;
        }

        return {
            empty: empty,
            push: push,
            pop: pop,
            top: top,
            size: size
        };
    };
// Definiujemy obiekt reprezentujący pojedyńczego klienta.

    var Klient = function (opt) {
        this.imie = opt.imie || '';
        this.nazwisko = opt.nazwisko || '';
        this.PESEL = opt.PESEL || '';
        this.stanKonta = opt.stanKonta || 0.0;
        this.historia = new Stack();

        this.wplac = function (kwota) {
            this.stanKonta += Math.abs(kwota);
            this.historia.push(Math.abs(kwota));
        };

        this.wyplac = function (kwota) {
            this.stanKonta -= Math.abs(kwota);
            this.historia.push(-1 * Math.abs(kwota));
        };

    };

// Definiujemy tablicę klientów

    var klienci = {};


// Dodajemy przykładowego klienta
    klienci[54122912875] = new Klient({
        imie: 'Pawel',
        nazwisko: 'Wal',
        PESEL: 54122912875
    });
    klienci[22222222222] = new Klient({
        imie: 'Patryk',
        nazwisko: 'Now',
        PESEL: 22222222222
    });
    klienci[11111111111] = new Klient({
        imie: 'Piotr',
        nazwisko: 'Kow',
        PESEL: 11111111111
    });
    klienci[12345678910] = new Klient({
        imie: 'Patrycja',
        nazwisko: 'Re',
        PESEL: 12345678910
    });
    klienci[1] = new Klient({
        imie: 'Patrycja',
        nazwisko: 'Re',
        PESEL: 1
    });

    //LOGOWANIE

    $login.on('click', function () {
        $login.val("");
    });

    var curUser;
    $logBtn.on('click', function () {
        //var str = $login.val();
        if (klienci[$login.val()]) {
            curUser = klienci[$login.val()];
            showUser(klienci[$login.val()]);
            this.disabled = true;
        } else {
            $logBtn.parent().append('<span>Podany pesel nie istnieje w bazie</span>');

        }
    });

    function showUser(curUser) {
        $userData.show();
        $container.find("span").remove();
        $userData.prepend('<span><h2>' + curUser.imie + ' ' + curUser.nazwisko + '</h2></span>');
        $userData.find('h1').html("Stan konta: " + curUser.stanKonta);
    }
    $('#deposit').on('click', function () {
        var amount = parseInt(prompt("Podaj kwotę:"));
        curUser.wplac(amount);
        console.log("Wpłacono " + amount + " zł");
        $userData.find('h1').html("Stan konta: " + curUser.stanKonta);
    });
    $('#withdraw').on('click', function () {
        var amount = parseInt(prompt("Podaj kwotę:"));
        curUser.wyplac(amount);
        $userData.find('h1').html("Stan konta: " + curUser.stanKonta);
    });
    $('#history').on('click', function () {
        // Klonujemy obiekt stosu, gdyż przy zdejmowaniu z niego stos się opróżni
        var tempHist = Object.create(curUser.historia);
        var histSize = tempHist.size();
        for (var i = 0; i < histSize; i++) {
            var tmpOper = tempHist.pop();
            if (tmpOper < 0)
                console.log("Wypłacono " + tmpOper + "PLN");
            else
                console.log("Wpłacono " + tmpOper + "PLN");
        }

    });
    //var currentUser = klienci[$login.val()];

    //REJESTRACJA
    $register.on('click', function () {

//        $('<input/>', {class: 'test', type: 'text', id: 'regName', value: 'imię', name: 'regName'}).on('click', function() {
//            console.log("To nie działa");
//        }).appendTo('#regBox');
//        $('<input/>', {type: 'text', id: 'regSname', value: 'nazwisko', name: 'regSname'}).appendTo('#regBox').on('click', function() {
//            //this.val("");
//        });
//        $('<input/>', {type: 'text', id: 'pesel', value: 'PESEL', name: 'pesel'}).appendTo('#regBox').on('click', function () {
//            //this.val("");
//        });
//        $('<button/>', {id: 'createNewUser'}).html('Utwórz').on('click', function () {
//            
//        }).appendTo('#regBox');
//        addEvents();
        $regBox.show();

        this.disabled = true;
    });

//    function addEvents() {
//        
//        $regName.on('click', function () {
//            console.log('tesssssst');
//        });
//        $('createNewUser').on('click', function () {
//            console.log('wow2');
//            
//        });
//    }
//
//
//
    $regName.on('focus', function () {
        $regName.val("");
    });
    $regSname.on('focus', function () {
        $regSname.val("");
    });
    $regPesel.on('focus', function () {
        $regPesel.val("");
    });
    $newUser.on('click', function () {
        var userPesel = parseInt($regPesel.val());
        if (klienci[userPesel]) {
            $newUser.parent().append('<span>Numer PESEL już istnieje</span>');
        } else {
            klienci[userPesel] = new Klient({
                imie: $regName,
                nazwisko: $regSname,
                PESEL: userPesel
            });
            console.log(klienci[userPesel]);
            this.disabled = true;
        }
    });



});