$(document).ready(function () {
//get element by selector
    var $wrap = $('.wrapper'), //przekazany wynik funkcji jQ
            pTxt = $wrap.children('p').html();
    pTxt = pTxt.split('.');
    //$wrap.children('h2').html(pTxt[0] + '.').css('color','red');
    $wrap.children('h2').html(pTxt[0] + '.').addClass('redTxt blueBorderBottom');
    //$wrap.children('h2').html(pTxt[0] + '.').css({'color':'red',
    //    'border-bottom' : '1px solid blue' });

    //ukrywamy box logowania
    //odniesienia do elementow przez jQuery w konwencji $zmienna
    var $loginBox = $('.loginBox'),
            n = 0;
    $loginBox.hide();
    $('#logMeIn').on('click', function () {
        $loginBox.is(':visible') ? $loginBox.hide() : $loginBox.show();
        n++;
        if (n == 5) {

            $wrap.append('<p>kliknąłeś już ' + n + " razy.</p>").children('p:last').css('color', 'blue');
            $(this).off('click'); //wewnatrz metody anonimowej mozemy uzyc this
        }
    });
    var $secPara = $('.secPara');
    //znajduje wszystkie elementy w $secPara
    $secPara.find('span').css('color', 'red');
    //console.log($secPara.children('input').val());
    $('.changeBtn').on('click', function () {
        $secPara.find('span').text($secPara.children('input').val());
    });
    //console.log($secPara.find('input').attr('name'));
    $secPara.find('input').attr('name', 'nowyName');
    //console.log($secPara.find('input').attr('name'));

    var users = [
        {name: 'John', pass: 'qwerty'},
        {name: 'Jane Doe', pass: 'unknown'}

    ];
    $secPara.children('input').on('keyup', function () {
        if ($(this).val().length > 3) {
            console.log($(this).val());
        }

    });
    $('#send').on('click', function () {
        var inpVal = $('#login').val();
        var pasVal = $('#password').val();
        for (var i = 0; i < users.length; i++) {
            if ((inpVal === users[i].name) && (pasVal === users[i].pass)) {
                $('.loginBox').children('p').text('Dane są poprawne.').css('color', 'green');
                return;
            }
        }
        $('.loginBox').children('p').text('Podano złe dane.').css('color', 'red');


    });

    ///////////////////po 13
    $('.firstBtn').on('click', function () {
        $('.wrap5').children('div').remove('.title');
    });
    $('.secondBtn').on('click', function () {
        var $infoDiv = $(this).parent().children('.info');
        console.log(($infoDiv).data('msg'));
        $infoDiv.data('msg', 'udało się');
        console.log(($infoDiv.data('msg')));
    });

    ///each
    var $myElems = $('ul').children('li');

    $myElems.each(function (index, elem) {
        //elem == this
        if (index % 2 == 1) {
            $(this).css({'background-color': 'yellow', 'color': 'red'});
        } else {
            $(elem).css({'background-color': 'black', 'color': 'white'});
        }
    });
    
    $('ol').children('li').each(function() {
        if(!$(this).hasClass('nono')) {
            $(this).addClass('redTxt');
        } else {
            $(this).addClass('blueBorderBottom');
        }
    });
    
});