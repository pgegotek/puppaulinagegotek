$(document).ready(function () {
    var $wrap = $('.wrapper'),
        $imie = $('#imie'),
        $nazwisko = $('#nazwisko'),
        $login = $('#login'),
        $haslo = $('#haslo'),
        $dataUr = $('#dataur'),
        $pesel = $('#pesel');
    $('button').on('click',function() {
        
        $wrap.find('span').remove();
        if($imie.val().length < 3) {
            $imie.addClass('redBorder');
            $imie.parent().append('<span> Podano wartość krótszą od 3</span>');
        } else {
            $imie.addClass('greenBorder');
            $imie.parent().find('span').remove();
        }
        if($nazwisko.val().length < 3) {
            $nazwisko.addClass('redBorder');
            $nazwisko.parent().append('<span> Podano wartość krótszą od 3</span>')
        } else {
            $nazwisko.addClass('greenBorder');
            $nazwisko.parent().find('span').remove();
        }
        if($login.val().length < 5 || $login.val().length > 12) {
            $login.addClass('redBorder');
            $login.parent().append('<span> Podano wartość krótszą od 5 lub większą od 12</span>')
        } else {
            $login.addClass('greenBorder');
            $login.parent().find('span').remove();
        }
        if($haslo.val().length < 9 || $haslo.val().length > 18) {
            $haslo.addClass('redBorder');
            $haslo.parent().append('<span> Podano wartość krótszą od 9 lub większą od 18</span>')
        } else {
            $haslo.addClass('greenBorder');
            $haslo.parent().find('span').remove();
        }
        if($dataUr.val().length != 10) {
            $dataUr.addClass('redBorder');
            $dataUr.parent().append('<span> Podano nieprawidłową wartość</span>')
        } else {
            $dataUr.addClass('greenBorder');
            $dataUr.parent().find('span').remove();
        }
        if($pesel.val().length != 11) {
            $pesel.addClass('redBorder');
            $pesel.parent().append('<span> Podano nieprawidłową wartość</span>')
        } else {
            $pesel.addClass('greenBorder');
            $pesel.parent().find('span').remove();
        }
    });
    
    
    
});