// Naszym kolejnym zadaniem będzie napisanie gry przeglądarkowej do zgadywania haseł. System losuje 1 słowo z zadeklarowanych, a następnie wyświetla słowo zakodowane w sposób: _ _ _ _
// Po prawej stronie znajdywać będą się litery, wygenerowane skryptowo (tj. umieszone automatycznie), po wciśnięciu których nastąpi odsłona litery w słowie, jeśli taka istnieje. Dodatkowo, jeżeli użytkownik wciśnie na klawiaturze wybrany klawisz, litera też powinna się odsłonić. Jeżeli litera została wciśnięta, przycisk powinien zostać zablokowany tak, aby nie można było go ponownie użyć dla wybranego słowa.
// Powyżej listy liter do wciśnięcia znajdywać się powinna lista użytych już liter.
// Liczba prób zgadywania: 2 * długość_słowa,
// Dodatkowo użytkownik ma możliwość 3 razy zgadnięcia hasła w inpucie, który znajduje się nad listą zgadywanych wyrazó.
// UWAGA! Przyjmij, że w strukturze html znajduje się tylko i wyłącznie (w sekcji body) <div class="fortune"></div>, reszta elementów tworzona jest przez skrypt.



$(document).ready(function() {
    /* tworzymy elementy */
    $('<div/>', { class : 'leftSide'}).prependTo('.fortune');
    $('<div/>', { class : 'rightSide'}).appendTo('.fortune');
    $('<div/>', { class : 'clear'}).appendTo('.fortune');
    $('<div/>', { class : 'guessBox' }).prependTo('.rightSide');
    
    $('<div/>', { class : 'counter' }).html('Ilość prób: <span></span>').appendTo('.rightSide');
    
    $('<div/>', { class : 'usedLettersBox' }).html('Użyte litery: <span class="usedLetters"></span>').appendTo('.rightSide');
    $('<div/>', { class : 'letterBox' }).appendTo('.rightSide');
    $('<input/>', { name : 'guess', id : 'guess', type : 'text' }).appendTo('.guessBox');
    $('<button/>', { id : 'guessBtn' }).html('Zgaduj').appendTo('.guessBox');
    $('<h1/>', { class : 'title' }).html('Koło fortuny').prependTo('.leftSide');
    $('<div/>', { class : 'phrase' }).html('_______').appendTo('.leftSide');
    
    /* generujemy buttony */
    for(var i = 65; i <= 90; i++) {
        var letter = String.fromCharCode(i);
        $('<button/>', { id : letter, class : 'guessLetter' }).html(letter).appendTo('.letterBox');
    }
    
    /* deklaracja słów */
    var words = ['oksymoron', 'jablko', 'banan', 'wrona', 'fajrant', 'urlop', 'przerwa', 'wolne', 'javascript'],
        currentWord = words[(Math.random() * words.length) | 0].toUpperCase(),
        dashedWord = "_".repeat(currentWord.length);
    console.log(currentWord);
    /* umieszczamy zakodowane slowo */
    $('.phrase').html(dashedWord);
    
    /* ustawiamy ilość prób */
    $('.counter').find('span').html(currentWord.length * 2);
    
    
    function guessLetterGame(e) {
        var currentLetter = (e.type == 'keyup') ? String.fromCharCode(e.keyCode) : $(this).attr('id'),
            splitedDashedWord = dashedWord.split(''),
            $counter = $('.counter').find('span'),
            currentCounter = parseInt($counter.html()),
            $usedLetters = $('.usedLetters'),
            letterExists = false;
        
        if(e.target.tagName == 'INPUT') {
            return;
        }
        
        for(var i = 0; i < dashedWord.length; i++) {
            if(currentWord.charAt(i) == currentLetter) {
                splitedDashedWord[i] = currentLetter;
                letterExists = true;
            }
        }
        dashedWord = splitedDashedWord.join('');
        $('.phrase').html(dashedWord);
        $counter.html(currentCounter - 1);
        if($usedLetters.html().length == 0) {
            $usedLetters.html(currentLetter);
        } else {
            $usedLetters.append(', ' + currentLetter);
        }
        $('#' + currentLetter).attr('disabled', 'disabled');
        if(letterExists) {
            $('#' + currentLetter).addClass('correctLetter');
        } else {
            $('#' + currentLetter).addClass('incorrectLetter');
        }
        
        /* sprawdzenie wygranej */
        if(checkForWin(dashedWord)) {
            return;   
        }        
        /* sprawdzenie przegranej */
        if(currentCounter == 1) {
            $('.guessLetter').off('click');
            $(document).off('keyup');
            $('.phrase').html('Przegrana (' + currentWord + ')');
            return;
        }
        
    }
    
    function checkForWin(word) {
        if(word == currentWord) {
            $('.guessLetter').off('click');
            $(document).off('keyup');
            $('.phrase').html('Wygrana (' + currentWord + ')').addClass('greenColor');
            return true;
        }
        return false;
    }
    
    
    /* nasluchujemy klikniecia i podmieniamy literki */
    $('.guessLetter').on('click', guessLetterGame);
    
    $(document).on('keyup', guessLetterGame);
    
    
    $('#guessBtn').on('click', function() {
        checkForWin($('#guess').val().toUpperCase());
    });
    
});

