var List = function(s) {
	var maxSize = s || 100;
	var elems = [];
	this.push_front = function(arg) {
		/* można użyć unshift(arg.toString())  */
		if(this.size() >= maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		elems.splice(0, 0, arg.toString());
	};
	this.push_back = function(arg) {
		if(this.size() >= maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		elems.splice(elems.length, 0, arg.toString());
		return this;
	};
	this.insert = function(ind, arg) {
		if(this.size() >= maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		elems.splice(ind, 0, arg.toString());
	};
	this.pop_front = function() {
		if(elems.length > 0) {
			var ret = elems[0];
			elems.shift(); /* usuwa pierwszy element, dosuwa inne */
			return ret;
		}
		return null;
	};
	this.pop_back = function() {
		if(elems.length > 0) {
			var ret = elems[elems.length-1];
			elems.pop();
			return ret;
		}
		return null;
	};
	this.size = function() {
		return (elems.length) ? elems.length : null;
	};
	this.max_size = function() {
		return maxSize;
	};
	this.empty = function() {
		return (elems.length === 0) ? true : false;
	};
	this.remove = function(arg) {
		for(var i=0;i<elems.length;i++) {
			if(elems[i] === arg.toString()) {
				elems.splice(i, 1);
                                --i;
			}
		}
	};
	this.sort = function() {
		elems.sort();
		return this;
	};
	this.reverse = function() {
		elems.reverse();
	};
	this.show = function() {
		return elems;
	}
};


var students = ['ania nowak','ala bosak','asia kozak','alina rodak','amelia polak','anastazja flisak','andżelika kowalska','aleksandra aleksandrowska','aurora zmierzch','basia baran'];

function school(tab) {
	var oList = new List(tab.length);
	for(var i in tab) {
		oList.push_back(tab[i]);
	}
	
	return oList;
}

var myStudents = school(students);
myStudents.sort();
var studentSize = myStudents.size();
for(var i = 0; i < studentSize; i++) {
	var stud = myStudents.pop_back();
	if(i % 2 == 0) console.log(stud);
}
/* zeby mozna bylo zrobic chaining, funkcje musza zwracac instancje na ktora zostaly wywolane, return this;  */
var list = new List();
var students1 = list.push_back('zenon').push_back('ania').push_back('pawel').sort().show();

console.log(students1);