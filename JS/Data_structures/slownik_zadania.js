var Dict = function() {
	var elems = {};
	this.has_Value = function(arg) {
		var keyTab = [];
		for(var key in elems) {
			if(elems[key] === arg) {
				keyTab.push(key);
			}
		}
		return (keyTab.length) ? keyTab : null;
	};
	this.has_Key = function(arg) {
		if( elems[arg] ) return true;
		return false;
	};
	
	this.add = function() {
		if(arguments.length == 2) {
			if(this.has_Key(arguments[0])) {
				return console.log('Ten klucz już istnieje. Użyj Update(), aby nadpisać');
			}
			elems[arguments[0]] = arguments[1];
		} else if(arguments[0] instanceof Array) {
			
			for(var i=0;i<arguments[0].length;i+=2) {
				if(this.has_Key(arguments[0][0])) {
					return console.log('Ten klucz już istnieje. Użyj Update(), aby nadpisać');
				}
				elems[arguments[0][i]] = arguments[0][i+1];
			}
		} else {
			for(var key in arguments[0]) {
				if(this.has_Key(key)) {
					return console.log('Ten klucz już istnieje. Użyj Update(), aby nadpisać');
				}
				elems[key] = arguments[0][key];
			}
		}
		//Object.keys(dict);
		return this;
	};
	
	this.remove = function(arg) {
		delete elems[arg];
		return this;
	};
	this.update = function(key, value) {
		if(this.has_Key(key)) {
			elems[key] = value;
		} else {
			console.log('taki klucz nie istnieje');
		}
		return this;
	};
	this.next = function(arg) {
		var keys = Object.keys(elems);
		keys = keys.sort();
		var nextKey;
		for(var i in keys) {
			if(keys[i] === arg) {
				var num = parseInt(i);
				nextKey = (num + 1)%keys.length;
			}
		}
		var obj = {};
		obj[keys[nextKey]] = elems[keys[nextKey]];
		return obj;
	};
	this.prev = function(arg) {
		var keys = Object.keys(elems);
		keys = keys.sort();
		var prevKey;
		for(var i in keys) {
			if(keys[i] === arg) {
				var num = parseInt(i);
				prevKey = (num === 0) ? keys.length-1 : (num - 1);
			}
		}
		var obj = {};
		obj[keys[prevKey]] = elems[keys[prevKey]];
		return obj;
	};
	this.value = function(arg) {
		if(this.has_Key(arg)) return elems[arg];
		console.log('taki klucz nie istnieje');
	};
	this.removeByValue = function(arg) {
		if(this.has_Value(arg)) {
			for(var key in elems) {
				if(elems[key] == arg) {
					delete elems[key];
				}
			}
			return this;
		}
		console.log('taka wartość nie istnieje');
	};
	this.sort = function() {
		var sorted = {};
		var ind = Object.keys(elems).sort();
		for(var i=0;i<ind.length;i++) {
			sorted[ind[i]] = elems[ind[i]];
		}
		return sorted;
	};
	this.show = function() {
		var tabElems = [];
		for(var key in elems) {
			tabElems.push([key ,elems[key] ]);
		}
		return tabElems;
	};
};

var words = new Dict();

var cooking_words = {
	"add a pinch of" : "dodać szczyptę",
	"bake" : "piec",
	"blend" : "zmiksować",
	"boil" : "ugotować",
	"break" : "rozbijać",
	"chop" : "siekać",
	"cut" : "kroić",
	"fry" : "usmażyć",
	"grate" : "trzeć na tarce",
	"grill" : "grillować"
	
};

var Dictionary = function(arg_words) {
	var words = new Dict();
	words.add(arg_words).show();
	this.translate = function(text) {
		var plText = '';
		text = text.split(' ');
		for(var i in text) {
			if(words.has_Key(text[i])) {
				plText += words.value(text[i]) + " ";
			} else {
				plText += '';
			}
		}
		return plText;
	};
};

var text = "bake, blend mix boil break";

var translator = new Dictionary(cooking_words);
translator.translate(text);
