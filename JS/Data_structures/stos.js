/* STOS */

var Stack = function() {
	var elems = [];
	function empty() {
		return (elems.length === 0) ? true : false;
	}
	function size() {
		return (elems.length) ? elems.length : null;
	}
	function push(arg) {
		elems.push(arg);
		return;
	}
	function pop() {
		if(elems.length >0) {
			var lastElem = elems[elems.length-1];
			elems.pop();
			return lastElem;
		} else {
			return null;
		}
	}
	function top() {
		return (elems.length === 0) ? null : elems.length-1;
	}
	
	return {
		empty : empty,
		push : push,
		pop : pop,
		top: top,
		size: size
	};
};

var oStack = new Stack();
oStack.push(5);
oStack.push(9);
console.log(oStack.size());
oStack.push(11);
console.log(oStack.top());
console.log(oStack.pop());
console.log(oStack.pop());
console.log(oStack.pop());
console.log(oStack.empty());
console.log(oStack.size());

console.log(oStack.elems);

/* ONP  */

function ONP(string) {
	var elems = string.split(' ');
	var oStack = new Stack();
	var operators = {
    '+': function(a, b) { return a + b; },
    '<': function(a, b) { return a < b; },
    '>': function(a, b) { return a > b; },
    '-': function(a, b) { return a - b; },
    '*': function(a, b) { return a * b; },
    '/': function(a, b) { return a / b; }
	};
	
	for(var i in elems) {
		if( !isNaN(parseInt(elems[i]))) {
			oStack.push(elems[i]);
		} else {
			var a = oStack.pop();
			var b = oStack.pop();
			oStack.push(operators[elems[i]](b, a));
		} 
	}
	

	return oStack.pop();
}




var elems = "2 7 + 3 / 14 3 - 4 * + 2 /";
ONP(elems);



function palindrome(string) {
	string = string.split(' ').join('').toUpperCase();
	var sWord = new Stack();
	var temp = [];
	for(var i in string) {
		sWord.push(string[i]);
	}
	for(var j=0; j<string.length;j++) {
		temp.push(sWord.pop());
	}
	temp = temp.join('');
	return (temp == string);
}

palindrome('żartem dano nadmetraż');
palindrome('Zagwiżdż i w gAz');
palindrome('A kilku tu klika');