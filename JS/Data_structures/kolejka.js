function Queue(s) {
	this.qSize = s || 100;
	this.elems = [];
	this.head = null;
	this.tail = 0;
}
Queue.prototype.empty = function() {
	return (this.elems.length === 0) ? true : false;
};
Queue.prototype.pop = function() {
	
	if(this.elems.length >0) {
		var firstElem = this.elems[0];
		this.elems.shift();
		this.tail--;
		return firstElem;
	} else {
		this.head = null;
		this.tail = null;
		return null;
	}
};
Queue.prototype.push = function(arg) {
		if(this.size() >= this.qSize) {
			return console.log("kolejka osiągnęła maksymalną długość");;
		}
		this.elems.push(arg);
		this.head = 0;
		this.tail++;
};
Queue.prototype.size = function() {
		return (this.elems.length) ? this.elems.length : null;
};

var oQueue = new Queue(4);

oQueue.push(4);
oQueue.push(12);
oQueue.push(45);

console.log(oQueue);
console.log(oQueue.size());
console.log(oQueue.empty());
console.log(oQueue.pop());
console.log(oQueue);
console.log(oQueue.tail);
oQueue.push(3);
oQueue.push(3);
console.log(oQueue);
oQueue.push(3);

/* LLIST */
