var List = function(s) {
	this.maxSize = s || 100;
	this.elems = ['banan', 'kiwi', 'japko'];
	this.push_front = function(arg) {
		if(this.size() >= this.maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		this.elems.splice(0, 0, arg);
	};
	this.push_back = function(arg) {
		if(this.size() >= this.maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		this.elems.splice(this.elems.length, 0, arg);
	};
	this.insert = function(ind, arg) {
		if(this.size() >= this.maxSize) {
			return console.log("lista osiągnęła maksymalną wielkość");
		}
		this.elems.splice(ind, 0, arg);
	};
	this.pop_front = function() {
		if(this.elems.length > 0) {
			var ret = this.elems[0];
			this.elems.shift();
			return ret;
		}
		return null;
	};
	this.pop_back = function() {
		if(this.elems.length > 0) {
			var ret = this.elems[this.elems.length-1];
			this.elems.pop();
			return ret;
		}
		return null;
	};
	this.size = function() {
		return (this.elems.length) ? this.elems.length : null;
	};
	this.max_size = function() {
		return this.maxSize;
	};
	this.empty = function() {
		return (this.elems.length === 0) ? true : false;
	};
	this.remove = function(arg) {
		for(var i=0;i<this.elems.length;i++) {
			if(this.elems[i] === arg) {
				this.elems.splice(i, 1);
                                --i;
			}
		}
	};
	this.sort = function() {
		this.elems.sort();
	};
	this.reverse = function() {
		this.elems.reverse();
	};
};
var oList = new List();
console.log(oList.sort());
oList.elems;