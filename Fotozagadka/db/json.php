<?php

class Place
{
	public $url;
	public $miejsce;
	public $opis;
}

$oPlace1 = new Place();
$oPlace1 -> url =  "https://goo.gl/dnJln9";
$oPlace1 -> miejsce = "miejsce 1";
$oPlace1 -> opis = "opis 1";

$oPlace2 = new Place();
$oPlace2 -> url =  "https://goo.gl/t3bQx3";
$oPlace2 -> miejsce = "miejsce 2";
$oPlace2 -> opis = "opis 2";

$oPlace3 = new Place();
$oPlace3 -> url =  "https://goo.gl/IKGVK8";
$oPlace3 -> miejsce = "miejsce 3";
$oPlace3 -> opis = "opis 3";

$oPlace4 = new Place();
$oPlace4 -> url =  "https://goo.gl/TDkbjv";
$oPlace4 -> miejsce = "miejsce 4";
$oPlace4 -> opis = "opis 4";

$arr = array(
	$oPlace1, $oPlace2, $oPlace3, $oPlace4			
);

echo json_encode($arr);
?>

