var map;
var markers = [];

function initMap() {
    var gdanskLoc = {lat: 54.355369, lng: 18.644794};
    var $map = $('#map');
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: gdanskLoc
    });

    //placeMarker(gdanskLoc);
    google.maps.event.addListener(map, 'click', function (event) {
        if (markers.length === 1) {
            markers[0].setMap(null);
            markers = [];
        }
        placeMarker(event.latLng);
    });
}

function placeMarker(location) {
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
}

function getCoords() {

}

$('.btn-success').on('click', function () {
    if (markers.length === 0) {
        alert("Nie ustawiłeś żadnej lokalizacji");
    } else {
        var guessCoords = new Coordinates(markers[0].getPosition().lat(), markers[0].getPosition().lng());
        //var coords = { lat: markers[0].getPosition().lat(), lng: markers[0].getPosition().lng()};
        console.log(guessCoords);
        console.log(getDistance(placeCoords, guessCoords));
    }

});

/*  OBJECT COORDS */
var Coordinates = function (lat, lng) {
    this.lat = lat;
    this.lng = lng;
};

var placeCoords = new Coordinates(54.349774, 18.654429);

var rad = function (x) {
    return x * Math.PI / 180;
};

var getDistance = function (p1, p2) {
    var R = 6378137; // Earth’s mean radius in meter
    var dLat = rad(p2.lat - p1.lat);
    var dLong = rad(p2.lng - p1.lng);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
            Math.sin(dLong / 2) * Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d; // returns the distance in meter
};

var previousPlaceIndex = 0;
var currentObject = {};


var $okBtn = $('.okBtn');

$okBtn.on('click', function () {
    var $nextGuessBtn = $('.nextGuessBtn');
    $.ajax({
        url: 'js/json.php',
        dataType: 'json',
        success: function (data) {
            return data;
        }

    });
    if (markers.length === 1) {
        $("<p>Odległość od prawidłowej odpowiedzi: " + getDistance(currentObject,currentObject) + "</p>").prependTo('.button-area');
        $okBtn.text("Następna zagadka");    
        //$("<button type='button' class='btn btn-success btn-large okBtn'>Następna zagadka</button>").appendTo('.button-area');
    }


    $nextGuessBtn.on('click', function () {
        $.ajax({
            url: 'js/json.php',
            dataType: 'json',
            success: function (data) {
                var index = getRandomPlaceNo(0, data.length);
                while (index === previousPlaceIndex) {
                    index = getRandomPlaceNo(0, data.length);
                }
                currenObject = data[index];
                $('.img-responsive').attr('src', data[index].url);
                previousPlaceIndex = index;
            }

        });
    });
});


function getRandomPlaceNo(min, max) {
    var number = Math.random() * (max - min) + min;
    return parseInt(number);
}

function initGame() {
    $.ajax({
        url: 'js/json.php',
        dataType: 'json',
        success: function (data) {
            var index = getRandomPlaceNo(0, data.length);
            while (index === previousPlaceIndex) {
                index = getRandomPlaceNo(0, data.length);
            }
            currenObject = data[index];
            $('.img-responsive').attr('src', data[index].url);
            previousPlaceIndex = index;
        }

    });
}
;





//$(document).ready = function () {

//};