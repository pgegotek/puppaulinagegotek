$(document).ready(function() {
	$('button').on('click', function() {
		var imie = $('#imie').val();
		var nazwisko = $('#nazwisko').val();
		
		var action = $(this).attr('id');
		$.ajax({
			url: 'json.php',
			dataType: 'json',
			
			data: {imie: imie, nazwisko: nazwisko, action: action },
			
			
			success: function(data) {
				if(data.status === 'ok') {
					if(action == "save") {
						$("<span>Zapisano<span/>").css("color","green").appendTo('body');	
					} else {
						$('.info').html('').css('color','black');
						$('<ul/>').appendTo('.info');
						for(var i in data.users) {
							if(data.users[i] != "")
								$('<li/>').html(data.users[i]).appendTo('ul');
							//$("<p>" + data[i] + "</p>").appendTo('body');
						}
					}
				}
				//console.log($('.list'));
				
				//var str = "";
				//for(var i in data) {
				//	str += data[i] + " ";
				//}
				//$('.info').text(str);
							
			}
		});
	});
});
