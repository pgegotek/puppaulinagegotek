# -*- coding: utf-8 -*-
from random import randint

elems = ["papier","nożyce","kamień"]
userScore = 0
computerScore = 0
result = [0,0,0]

def getValues():
	userElem = raw_input("Co wybierasz? p - papier, n - nożyce, k - kamień ")
	wrongValue = True
	while(wrongValue):
		if not(userElem == "k" or userElem == "p" or userElem == "n"):
			print "zła wartość, jeszcze raz."
			userElem = raw_input("Co wybierasz? p - papier, n - nożyce, k - kamień ")
		else: 
			wrongValue = False
	if(userElem == 'k'):
		userElem = elems[2]
	elif userElem == 'n':
		userElem = elems[1]
	else:
		userElem = elems[0]

	computerElem = elems[randint(0,2)]
	print userElem, computerElem

	return [userElem, computerElem]


def checkResult(userElem, computerElem, elems):
	if(userElem == elems[0]):
		if(computerElem == elems[0]):
			return "even"
		elif(computerElem == elems[1]):
			return "computerWins"
		else:
			return "userWins"
	elif(userElem == elems[1]):
		if(computerElem == elems[0]):
			return "userWins"
		elif(computerElem == elems[1]):
			return "even"
		else:
			return "computerWins"
	else:
		if(computerElem == elems[0]):
			return "computerWins"
		elif(computerElem == elems[1]):
			return "userWins"
		else:
			return "even"



def checkWin(values, elems):
	score = checkResult(values[0],values[1], elems)
	if score == "computerWins":
		global computerScore
		computerScore = computerScore + 1
	elif score == "userWins":
		global userScore
		userScore = userScore + 1
	return [score, userScore, computerScore]


def showMsg(values):
	print ("Wybrałeś " + values[0] + ", komputer wybrał " + values[1])
	result = checkWin(values, elems)
	print ("Wynik rundy: " + str(result[0]) + " Punkty użytkownika: " + str(result[1]) + " Punkty komputera: " + str(result[2]))	
	return result

def play():
	values = getValues()
	result = showMsg([values[0],values[1]])
	return result

"""
GRA
"""

while(result[1] !=3 and result[2] !=3):
	result = play()

if(result[1] ==3):
	print "Użytkownik wygrał"
else:
	print "Komputer wygrał"



	
