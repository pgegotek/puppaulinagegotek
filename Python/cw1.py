# -*- coding: utf-8 -*-

def czy_liczba(a):
	if unicode(a, 'utf-8').isnumeric() == True:
		return True
	elif any(c.isalpha() for c in a):
		return False
	else:
		return True


def calcPower():
		
	x = raw_input("Podstawa: ")
	y = raw_input("Wykładnik: ")
	
	if czy_liczba(x) == True and czy_liczba(y) == True:
		return float(x)**float(y)
	else:
		return str(x)+"^"+str(y)

print calcPower()


